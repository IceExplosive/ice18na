package ice18na.action

import com.intellij.codeInsight.intention.impl.BaseIntentionAction
import com.intellij.ide.highlighter.HtmlFileType
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiFile
import com.intellij.psi.util.PsiTreeUtil

class ParseKeysActions : BaseIntentionAction() {

    override fun getFamilyName(): String {
        return "Parse translation keys"
    }

    override fun isAvailable(project: Project, editor: Editor?, file: PsiFile?): Boolean {
        Angular2HtmlFileType
        return file?.fileType == Angular2::class.java
    }

    override fun invoke(project: Project, editor: Editor?, file: PsiFile?) {
        if (editor == null || file == null) return

//        PsiTreeUtil.collectElementsOfType(file, NgPipeEx)
    }

}
